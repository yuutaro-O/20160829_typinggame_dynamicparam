#include <string>

//ロケール変更失敗時のエラー
#define LOCALE_CHANGE_ERROR 0x10000000
//日本ロケールを表す文字列
#define JAPANESE_LOCALEMSG "Japanese_Japan.932"
/*
DynamicParamクラス
メンバ関数
+ string HiraToRoma(string);
	└引数		:string ひらがなの文字列
	　戻り値	:string 引数をローマ字に変換した文字列
	  動作		:引数として受け取ったひらがなをローマ字に変換、
				 戻り値として変換後の文字列を返す関数
				 タイピングゲームに使用する想定
+ int DynamicParam_Init();
	└引数		:なし
	　戻り値	:int 0…正常	0以外…エラー
	  動作		:C++上で日本語を正しく扱う為に初期設定を行う。
*/
using namespace std;

class DynamicParam {
private:
	/*このクラスを用いたインスタンスを作成できないように、
	コンストラクタとデストラクタを秘匿*/
	DynamicParam();
	~DynamicParam();
	
public:
	//地域設定の初期設定を行う
	static int DynamicParam_Init();
	//ひらがなをローマ字に変換する
	static string HiraToRoma(string);
};